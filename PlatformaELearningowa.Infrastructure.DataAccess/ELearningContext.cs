﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Infrastructure.DataAccess.DbEntities;

namespace PlatformaELearningowa.Infrastructure.DataAccess
{
    class ELearningContext : DbContext
    {
		public DbSet<Announcement> Announcements { get; set; }
		public DbSet<Course> Courses { get; set; }
		public DbSet<File> Documents { get; set; }
		public DbSet<Group> Groups { get; set; }
		public DbSet<Message> Messages { get; set; }
		public DbSet<Project> Projects { get; set; }
		public DbSet<Question> Questions { get; set; }
		public DbSet<Topic> Topics { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<User> Users { get; set; }
	}
}
