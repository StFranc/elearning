﻿using System;
using System.Collections.Generic;
using System.Text;
using PlatformaELearningowa.Domain.DataAccess;

namespace PlatformaELearningowa.Infrastructure.DataAccess.Repositories
{
	public abstract class Repository<T> : IRepository<T> where T : class
	{
		public Repository()
		{

		}

		public void Add(T entity)
		{
			throw new NotImplementedException();
		}

		public void AddRange(IEnumerable<T> entities)
		{
			throw new NotImplementedException();
		}

		public T Get(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<T> GetAll()
		{
			throw new NotImplementedException();
		}

		public IEnumerable<T> GetPage(int pageSize, int pageNumber)
		{
			throw new NotImplementedException();
		}

		public void Remove(T entity)
		{
			throw new NotImplementedException();
		}

		public void RemoveRange(IEnumerable<T> entites)
		{
			throw new NotImplementedException();
		}
	}
}
