﻿using System;
using System.Collections.Generic;
using System.Text;
using PlatformaELearningowa.Domain.DataAccess;
using PlatformaELearningowa.Domain.Entities;

namespace PlatformaELearningowa.Infrastructure.DataAccess.Repositories
{
    public class CoursesRepository : Repository<Course>, ICoursesRepository
    {
    }
}
