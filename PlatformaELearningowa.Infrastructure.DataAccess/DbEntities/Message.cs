﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Infrastructure.DataAccess.DbEntities
{
    class Message
    {
		public int? Id { get; private set; }

		public string Content { get; private set; }

		public DateTime SentDate { get; private set; }
	}
}
