﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Infrastructure.DataAccess.DbEntities
{
    class Project
    {
		public int? Id { get; set; }

		public string PrivateComment { get; set; }

		public string PublicComment { get; set; }

		public ICollection<File> Documents { get; set; }
	}
}
