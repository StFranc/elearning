﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlatformaELearningowa.Infrastructure.DataAccess.DbEntities
{
    class Course
    {
		public int? Id { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }
		
		public ICollection<User> ParticipantIds { get; set; }

		public ICollection<File> Documents { get; set; }
	}
}
