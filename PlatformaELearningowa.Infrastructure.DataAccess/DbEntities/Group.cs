﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Infrastructure.DataAccess.DbEntities
{
    class Group
    {
		public int? Id { get; set; }
		
		public ICollection<User> Members { get; set; }
	}
}
