﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlatformaELearningowa.Infrastructure.DataAccess.DbEntities
{
    class User
    {
		public string Id { get; set; }

		public string Email { get; set; }
		
		public ICollection<Role> Roles { get; set; }
	}
}
