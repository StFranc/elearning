﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Infrastructure.DataAccess.DbEntities
{
    class File
    {
		public int? Id { get; set; }

		public string Name { get; set; }

		public byte[] File { get; set; }
	}
}
