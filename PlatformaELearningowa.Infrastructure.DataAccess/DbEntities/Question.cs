﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Infrastructure.DataAccess.DbEntities
{
    class Question
    {
		public int? Id { get; set; }

		public string Title { get; set; }
		
		public ICollection<Message> Messages { get; set; }
	}
}
