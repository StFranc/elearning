﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Infrastructure.DataAccess.DbEntities
{
    class Topic
    {
		public int? Id { get; set; }

		public string Description { get; set; }
		
		public ICollection<File> Documents { get; set; }

	}
}
