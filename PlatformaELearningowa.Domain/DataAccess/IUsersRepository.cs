﻿using System;
using System.Collections.Generic;
using System.Text;
using PlatformaELearningowa.Domain.Entities;

namespace PlatformaELearningowa.Domain.DataAccess
{
    public interface IUsersRepository : IRepository<User>
    {
    }
}
