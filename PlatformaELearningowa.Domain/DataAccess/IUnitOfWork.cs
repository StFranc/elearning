﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Domain.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
		ICoursesRepository CoursesRepository { get; }

		DataAccessResult Complete();
    }
}
