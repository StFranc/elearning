﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Domain.Application
{
    public interface INotificationManager
    {
		bool CriticalErrorOccurred { get; }

		void AddNewNotification(string content, NotificationType type, bool criticalErrorOccurred);

		string GetLastNotification();
	}

	public enum NotificationType { Success, Error, Warning, Info }
}
