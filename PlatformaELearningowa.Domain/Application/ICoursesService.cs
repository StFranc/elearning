﻿using System;
using System.Collections.Generic;
using System.Text;
using PlatformaELearningowa.Domain.Entities;

namespace PlatformaELearningowa.Domain.Application
{
    public interface ICoursesService
    {
		IEnumerable<Course> GetAllCourses(int pageSize, int pageNumber);

		IEnumerable<Course> GetUserCourses(string currentUserId, int pageSize, int pageNumber);

		Course GetSpecificCourse(int courseId, string currentUserId);
    }
}
