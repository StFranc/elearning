﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PlatformaELearningowa.Domain.DataAccess;
using PlatformaELearningowa.Domain.Entities;

namespace PlatformaELearningowa.Domain.Application
{
	public class CoursesService : ICoursesService
	{
		readonly IUnitOfWork unitOfWork;

		public CoursesService(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		public IEnumerable<Course> GetAllCourses(int pageSize, int pageNumber)
		{
			IEnumerable<Course> result = null;
			result = unitOfWork.CoursesRepository.GetPage(pageSize, pageNumber);
			unitOfWork.Dispose();
			return result;
		}

		public Course GetSpecificCourse(int courseId, string currentUserId)
		{
			if (String.IsNullOrWhiteSpace(currentUserId))
			{
				throw new NullReferenceException("currentUserId not specified");
			}
			Course result = null;
			var course = unitOfWork.CoursesRepository.Get(courseId);
			if (course.ParticipantIds.Contains(currentUserId))
			{
				result = course;
			}
			return result;
		}

		public IEnumerable<Course> GetUserCourses(string currentUserId, int pageSize, int pageNumber)
		{
			if (String.IsNullOrWhiteSpace(currentUserId))
			{
				throw new NullReferenceException("currentUserId not specified");
			}
			IEnumerable<Course> result = null;

			return result;
		}
	}
}
