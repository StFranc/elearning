﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlatformaELearningowa.Domain.Entities
{
    public class Course
    {
		public int? Id { get; private set; }

		public string Name { get; private set; }

		public string Description { get; private set; }

		private List<string> participantIds;
		public IReadOnlyCollection<string> ParticipantIds => participantIds;

		private List<File> documents;
		public IReadOnlyCollection<File> Documents => documents.AsReadOnly();
	}
}
