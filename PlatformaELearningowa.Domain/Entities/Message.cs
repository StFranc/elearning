﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Domain.Entities
{
    public class Message
    {
		public int? Id { get; private set; }

		public string Content { get; private set; }

		public DateTime SentDate { get; private set; }
	}
}
