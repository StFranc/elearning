﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Domain.Entities
{
    public class Role
    {
		public int? Id { get; private set; }

		public string Name { get; private set; }
	}
}
