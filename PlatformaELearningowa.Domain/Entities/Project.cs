﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Domain.Entities
{
    public class Project
    {
		public int? Id { get; private set; }

		public string PrivateComment { get; private set; }

		public string PublicComment { get; private set; }

		private List<File> documents;

		public IReadOnlyList<File> Documents => documents.AsReadOnly();
	}
}
