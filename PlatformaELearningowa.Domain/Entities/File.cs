﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Domain.Entities
{
    public class File
    {
		public int? Id { get; private set; }

		public string Name { get; private set; }

		private byte[] file;
		public byte[] File => file.Clone() as byte[];
	}
}
