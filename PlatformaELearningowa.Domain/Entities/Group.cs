﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Domain.Entities
{
    public class Group
    {
		public int? Id { get; private set; }

		private List<User> members;
		public IReadOnlyList<User> Members => members.AsReadOnly();
	}
}
