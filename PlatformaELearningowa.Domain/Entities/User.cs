﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlatformaELearningowa.Domain.Entities
{
    public class User
    {
		public string Id { get; private set; }

		public string Email { get; private set; }

		private List<Role> roles;
		public IReadOnlyList<Role> Roles => roles.AsReadOnly();
	}
}
