﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Domain.Entities
{
    public class Topic
    {
		public int? Id { get; private set; }

		public string Description { get; private set; }

		private List<File> documents;
		public IReadOnlyList<File> Documents => documents.AsReadOnly();

	}
}
