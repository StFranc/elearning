﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlatformaELearningowa.Domain.Entities
{
    public class Question
    {
		public int? Id { get; private set; }

		public string Title { get; private set; }

		private List<Message> messages;
		public IReadOnlyList<Message> Messages => messages.AsReadOnly();
	}
}
