﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Services
{
    public interface ICoursesService
    {
		CourseDetailedVM GetCourse(int courseId, string userId);

		CourseWriteVM GetCourseForEditing(int courseId, string userId);

		IEnumerable<CourseSimpleVM> GetCourses(int pageNumber, int pageSize);

		IEnumerable<CourseSimpleVM> GetAttendedCourses(int pageNumber, int pageSize, string userId);

		void AddCourse(CourseWriteVM course);

		void EditCourse(CourseWriteVM course);

		void RemoveCourse(int courseId, string userId);

		void EnrollToCourse(string enrollmentKey, string userId, int courseId);
    }
}
