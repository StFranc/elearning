﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Topics;

namespace PlatformaELearningowa.Web.Services
{
    public interface ITopicsService
    {
		IEnumerable<TopicSimpleVM> GetCourseTopics(int courseId);

		TopicDetailedVM GetTopicDetails(int courseId, int topicId);

		TopicWriteVM GetTopicForEdition(int courseId, int topicId);

		void AddNewTopic(TopicWriteVM topic);

		void EditExisitngTopic(TopicWriteVM topic);

		void DeleteTopic(int courseId, int topicId);

		void ChangeTopicNumber(int courseId, int topicId, int newNumber);
    }
}
