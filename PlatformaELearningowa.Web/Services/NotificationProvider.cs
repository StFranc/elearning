﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Services
{
	public class NotificationProvider : INotificationProvider
	{
		public string GetNotificationForEvent(EventType eventType)
		{
			string result = null;
			switch (eventType)
			{
				case EventType.DbReadError:
					result = "Wystąpił błąd przy odczycie z bazy danych";
					break;
				case EventType.DbWriteError:
					result = "Wystąpił błąd przy zapisie do bazy danych";
					break;
			}
			return result;
		}
	}
}
