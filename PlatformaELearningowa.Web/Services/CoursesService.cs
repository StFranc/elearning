﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Factories;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;
using PlatformaELearningowa.Web.Models;
using AutoMapper;

namespace PlatformaELearningowa.Web.Services
{
	public class CoursesService : ICoursesService
	{
		private readonly ELearningContext db;
		private readonly ICoursesFactory coursesFactory;
		private readonly INotificationManager notificationManager;


		public CoursesService(
			ELearningContext db,
			ICoursesFactory coursesFactory,
			INotificationManager notificationManager)
		{
			this.db = db;
			this.coursesFactory = coursesFactory;
			this.notificationManager = notificationManager;
		}

		public void AddCourse(CourseWriteVM courseVm)
		{
			try
			{
				var course = coursesFactory.CreateCourse(courseVm);
				var userToCourse = coursesFactory.CreateUserToCourseLink(courseVm.CreatorId, 0);
				course.AssociatedUsers.Add(userToCourse);
				userToCourse.Course = course;
				db.Attach<Course>(course).State = EntityState.Added;
				db.Attach<UserToCourse>(userToCourse).State = EntityState.Added;
				db.SaveChanges();
			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void EditCourse(CourseWriteVM courseVm)
		{
			try
			{
				var course = coursesFactory.CreateCourse(courseVm);
				db.Attach<Course>(course).State = EntityState.Modified;
				db.SaveChanges();
			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public IEnumerable<CourseSimpleVM> GetCourses(int pageNumber, int pageSize)
		{
			IEnumerable<CourseSimpleVM> result = null;
			try
			{
				result = db.Courses
					.Skip((pageNumber - 1) * pageSize)
					.Take(pageSize)
					.AsEnumerable()
					.Select(c => coursesFactory.CreateSimpleViewModel(c));
			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbReadError, true);
				throw;
			}
			return result;
		}

		public IEnumerable<CourseSimpleVM> GetAttendedCourses(int pageNumber, int pageSize, string userId)
		{
			IEnumerable<CourseSimpleVM> result = null;
			try
			{
				var userCourses = db.UsersToCourses
					.Where(uc => uc.UserId.Equals(userId))
					.Select(uc => uc.Course)
					.ToList();
				var userGroupsCourses = db.UsersToGroups
					.Where(ug => ug.UserId.Equals(userId))
					.SelectMany(ug => ug.Group.AssociatedCourses)
					.Select(gc => gc.Course)
					.ToList();

				result = userCourses
					.Union(userGroupsCourses)
					.Distinct()
					.Select(c => coursesFactory.CreateSimpleViewModel(c));
			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbReadError, true);
				throw;
			}
			return result;
		}

		public CourseDetailedVM GetCourse(int courseId, string userId)
		{
			CourseDetailedVM result = null;
			try
			{
				var dbResult = db.Courses
					.Where(c => c.CourseId == courseId)
					.Include(c => c.AssociatedUsers)
					.Include(c => c.Topics)
					.Include(c => c.Files)
					.Include(c => c.Announcements)
					.FirstOrDefault();

				var dbGroups = db.UsersToGroups
					.Where(ug => ug.UserId.Equals(userId))
					.SelectMany(ug => ug.Group.AssociatedCourses)
					.Where(gc => gc.CourseId == courseId);

				// Second query executes only when no direct link between user and course found
				if (dbResult.AssociatedUsers.Any(au => au.UserId.Equals(userId)) || dbGroups.Any())
				{
					dbResult.Files = dbResult.Files
						.Where(f => f.RelatedTopicId == null)
						.ToList();
					result = coursesFactory.CreateDetailedViewModel(dbResult);
				}

			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbReadError, true);
			}
			return result;
		}

		public void RemoveCourse(int courseId, string userId)
		{
			try
			{
				var toRemove = db.UsersToCourses
					.Include(uc => uc.Course)
					.FirstOrDefault(uc => uc.CourseId == courseId && uc.UserId == userId)?
					.Course;

				if(toRemove != null)
				{
					db.Entry<Course>(toRemove).State = EntityState.Deleted;
					db.SaveChanges();
					notificationManager.AddNotification("Kurs pomyślnie usunięty.", NotificationType.Success);
				}
				else
				{
					notificationManager.AddNotification("Nie masz uprawnień do usunięcia tego kursu.", true);
				}
			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public CourseWriteVM GetCourseForEditing(int courseId, string userId)
		{
			CourseWriteVM result = null;
			try
			{
				var dbResult = db.UsersToCourses
					.Include(uc => uc.Course)
					.FirstOrDefault(uc => uc.CourseId == courseId && uc.UserId == userId)?
					.Course;
				result = coursesFactory.CreateWriteViewModel(dbResult);
			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbReadError, true);
				throw;
			}
			return result;
		}

		public void EnrollToCourse(string enrollmentKey, string userId, int courseId)
		{
			try
			{
				var dbCourse = db.Courses
					.Include(c => c.AssociatedUsers)
					.FirstOrDefault(c => c.CourseId == courseId);

				if(dbCourse == null)
				{
					notificationManager.AddNotification("Podany kurs nie istnieje", NotificationType.Error);
				}
				else if (dbCourse.AssociatedUsers.Any(uc => uc.UserId.Equals(userId)))
				{
					notificationManager.AddNotification("Jesteś już zapisany do tego kursu", NotificationType.Error);
				}
				else if (enrollmentKey != dbCourse.EnrollmentKey)
				{
					notificationManager.AddNotification("Nieprawidłowy klucz", NotificationType.Error);
				}
				else
				{
					var userToCourse = coursesFactory.CreateUserToCourseLink(userId, courseId);
					db.UsersToCourses.Attach(userToCourse).State = EntityState.Added;
					db.SaveChanges();
					notificationManager.AddNotification("Udało się pomyślnie zapisać do kursu.", NotificationType.Success);
				}
			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}
	}
}
