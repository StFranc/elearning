﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PlatformaELearningowa.Web.Models.ViewModels.Common;

namespace PlatformaELearningowa.Web.Services
{
    public interface IFilesService
	{
		void AddCourseFile(IFormFile formFile, int courseId, string userId, int? topicId = null);

		(byte[] file, string contentType) GetCourseFile(int fileId, int courseId, string userId);

		void DeleteFile(int fileId, string userId);

		void AddProjectFile(IFormFile formFile, int projectId, string userId);

		(byte[] file, string contentType) GetProjectFile(int projectId, string userId);

		void DeleteProjectFile(int projectId, string userId);
	}
}
