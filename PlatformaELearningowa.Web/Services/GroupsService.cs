﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Web.Models;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;
using PlatformaELearningowa.Web.Models.ViewModels.Groups;

namespace PlatformaELearningowa.Web.Services
{
    public class GroupsService : IGroupsService
    {
		private readonly IMapper mapper;
		private readonly INotificationManager notificationManager;
		private readonly ELearningContext db;
		private readonly UserManager<User> userManager;

		public GroupsService(IMapper mapper,
			ELearningContext db,
			INotificationManager notificationManager,
			UserManager<User> userManager)
		{
			this.mapper = mapper;
			this.notificationManager = notificationManager;
			this.db = db;
			this.userManager = userManager;
		}

		public void AddGroup(GroupWriteVM model, string userId)
		{
			try
			{
				model.GroupId = 0;
				var dbGroup = mapper.Map<Group>(model);
				var userToGroup = new UserToGroup { Group = dbGroup, UserId = userId };
				db.Entry<UserToGroup>(userToGroup).State = EntityState.Added;
				db.Entry<Group>(dbGroup).State = EntityState.Added;
				db.SaveChanges();
				notificationManager
					.AddNotification("Grupa pomyślnie stworzona.", NotificationType.Success);
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void AssociateCourse(int groupId, int courseId)
		{
			try
			{
				var dbGroup = db.Groups.FirstOrDefault(g => g.GroupId == groupId);
				var dbCourse = db.Courses.FirstOrDefault(c => c.CourseId == courseId);
				if(dbGroup == null)
				{
					notificationManager
						.AddNotification("Podana grupa nie istnieje.", true);
				}
				else if(dbCourse == null)
				{
					notificationManager
						.AddNotification("Podany kurs nie istnieje.", true);
				}
				else
				{
					var dbGroupToCourse = db.GroupsToCourses
						.FirstOrDefault(gc => gc.GroupId == groupId && gc.CourseId == courseId);

					if(dbGroupToCourse == null)
					{
						dbGroupToCourse = new GroupToCourse
						{ GroupId = groupId, CourseId = courseId };
						db.Entry<GroupToCourse>(dbGroupToCourse).State = EntityState.Added;

						db.SaveChanges();
						notificationManager
							.AddNotification("Grupa pomyslnie powiązana z kursem.", NotificationType.Success);
					}
					else
					{
						notificationManager
							.AddNotification("Grupa jest już powiązana z tym kursem.", true);
					}
				}
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void AssociateUser(int groupId, string userId)
		{
			try
			{
				var dbGroup = db.Groups.FirstOrDefault(g => g.GroupId == groupId);
				var dbUser = userManager.FindByIdAsync(userId).Result;

				if (dbGroup == null)
				{
					notificationManager
						.AddNotification("Podana grupa nie istnieje.", true);
				}
				else if (dbUser == null)
				{
					notificationManager
						.AddNotification("Podany użytkownik nie istnieje.", true);
				}
				else
				{
					var dbUserToGroup = db.UsersToGroups
						.FirstOrDefault(ug => ug.GroupId == groupId && ug.UserId.Equals(userId));

					if (dbUserToGroup == null)
					{
						dbUserToGroup = new UserToGroup
						{ GroupId = groupId, UserId = userId };
						db.Entry<UserToGroup>(dbUserToGroup).State = EntityState.Added;

						db.SaveChanges();
						notificationManager
							.AddNotification("Użytkownik pomyślnie dodany do grupy.", NotificationType.Success);
					}
					else
					{
						notificationManager
							.AddNotification("Użytkownik jest już powiązany z tą grupą.", true);
					}
				}
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void DeleteGroup(int groupId, string userId)
		{
			try
			{
				var toDelete = db.UsersToGroups
					.Where(ug => ug.GroupId == groupId && ug.UserId.Equals(userId))
					.Select( ug => ug.Group)
					.FirstOrDefault();
				if(toDelete == null)
				{
					notificationManager
						   .AddNotification("Podana grupa nie istnieje.", true);
				}
				else
				{
					db.Entry<Group>(toDelete).State = EntityState.Deleted;
					db.SaveChanges();
					notificationManager
						.AddNotification("Grupa usunięta pomyślnie.", NotificationType.Success);
				}
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public GroupWriteVM GetGroupForEdition(int groupId, string userId)
		{
			GroupWriteVM result = null;
			try
			{
				var dbGroup = db.UsersToGroups
					.Where(ug => ug.GroupId == groupId && ug.UserId.Equals(userId))
					.Select(ug => ug.Group)
					.FirstOrDefault();
				if (dbGroup == null)
				{
					notificationManager
						.AddNotification("Grupa o podanym Id nie istnieje.", true);
				}
				else
				{

					var groupCourses = db.GroupsToCourses
						.Where(gc => gc.GroupId == groupId)
						.Select(gc => gc.Course)
						.ToList();
					var otherCourses = db.Courses
						.ToList()
						.Except(groupCourses);

					result = mapper.Map<GroupWriteVM>(dbGroup);
					result.AssociatedCourses = mapper.Map<List<CourseSimpleVM>>(groupCourses);
					result.AvailableCourses = mapper.Map<List<CourseSimpleVM>>(otherCourses);
				}
			}
			catch (Exception)
			{
				notificationManager
					.AddNotification(EventType.DbReadError, true);
				throw;
			}
			return result;
		}

		public IEnumerable<GroupSimpleVM> GetGroups(string userId)
		{
			IEnumerable<GroupSimpleVM> result = null;
			try
			{
				result = db.UsersToGroups
					.Where(ug => ug.UserId.Equals(userId))
					.Select(ug => ug.Group)
					.ToList()
					.Select(g => mapper.Map<GroupSimpleVM>(g));
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbReadError, true);
				throw;
			}
			return result;
		}

		public void ModifyGroup(GroupWriteVM model, string userId)
		{
			try
			{
				var toModify = db.UsersToGroups
					.Where(ug => ug.GroupId == model.GroupId && ug.UserId.Equals(userId))
					.Select(ug => ug.Group)
					.FirstOrDefault();
				mapper.Map<GroupWriteVM, Group>(model, toModify);
				db.Entry<Group>(toModify).State = EntityState.Modified;
				db.SaveChanges();
				notificationManager
					.AddNotification("Grupa pomyślnie zedytowana.", NotificationType.Success);
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void UnassociateCourse(int groupId, int courseId)
		{
			try
			{
				var dbGroupToCourse = db.GroupsToCourses
					.FirstOrDefault(gc => gc.GroupId == groupId && gc.CourseId == courseId);
				if(dbGroupToCourse == null)
				{
					notificationManager
						.AddNotification("Grupa nie jest powiązana z kursem.", true);
				}
				else
				{
					db.Entry<GroupToCourse>(dbGroupToCourse).State = EntityState.Deleted;
					db.SaveChanges();
					notificationManager
						.AddNotification("Grupa pomyślnie usunięta z kursu.", NotificationType.Success);
				}
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void UnassociateUser(int groupId, string userId)
		{
			try
			{
				var dbUserToGroup = db.UsersToGroups
					.FirstOrDefault(ug => ug.GroupId == groupId && ug.UserId == userId);
				if (dbUserToGroup == null)
				{
					notificationManager
						.AddNotification("Użytkownik nie jest powiązany z grupą.", true);
				}
				else
				{
					db.Entry<UserToGroup>(dbUserToGroup).State = EntityState.Deleted;
					db.SaveChanges();
					notificationManager
						.AddNotification("Użytkownik pomyślnie usunięty z grupy.", NotificationType.Success);
				}
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}
	}
}
