﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Groups;

namespace PlatformaELearningowa.Web.Services
{
    public interface IGroupsService
    {
		void DeleteGroup(int groupId, string userId);
		void AddGroup(GroupWriteVM model, string userId);
		void ModifyGroup(GroupWriteVM model, string userId);
		IEnumerable<GroupSimpleVM> GetGroups(string userId);
		void AssociateUser(int groupId, string userId);
		void UnassociateUser(int groupId, string userId);
		void AssociateCourse(int groupId, int courseId);
		void UnassociateCourse(int groupId, int courseId);
		GroupWriteVM GetGroupForEdition(int groupId, string userId);
	}
}
