﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Services
{
	public class NotificationManager : INotificationManager
	{
		private readonly Stack<(string, NotificationType)> notifications;
		private readonly INotificationProvider notificationProvider;

		public NotificationManager(INotificationProvider notificationProvider)
		{
			notifications = new Stack<(string, NotificationType)>();
			this.notificationProvider = notificationProvider;
		}

		private bool criticalErrorOcurred;
		public bool CriticalErrorOccured => criticalErrorOcurred;

		public void AddNotification(string content, NotificationType type)
		{
			var notification = (content, type);
			notifications.Push(notification);
		}

		public void AddNotification(string content, bool criticalError)
		{
			if(criticalError)
				criticalErrorOcurred = true;
			AddNotification(content, criticalError ? NotificationType.Error : NotificationType.Info);
		}

		public (string content, NotificationType type) GetLastNotification()
		{
			return notifications.Peek();
		}

		public void AddNotification(EventType eventType, NotificationType notificationType)
		{
			var content = notificationProvider.GetNotificationForEvent(eventType);
			AddNotification(content, notificationType);
		}

		public void AddNotification(EventType eventType, bool criticalError)
		{
			var content = notificationProvider.GetNotificationForEvent(eventType);
			AddNotification(content, criticalError);
		}
	}
}
