﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Web.Factories;
using PlatformaELearningowa.Web.Models;
using PlatformaELearningowa.Web.Models.Entities;

namespace PlatformaELearningowa.Web.Services
{
	public class FilesService : IFilesService
	{
		private readonly ELearningContext db;
		private readonly IFilesFactory filesFactory;
		private readonly INotificationManager notificationManager;
		private readonly IMapper mapper;

		public FilesService(
			ELearningContext db,
			IFilesFactory filesFactory,
			INotificationManager notificationManager,
			IMapper mapper)
		{
			this.db = db;
			this.filesFactory = filesFactory;
			this.notificationManager = notificationManager;
			this.mapper = mapper;
		}

		public void AddCourseFile(IFormFile formFile, int courseId, string userId, int? topicId = null)
		{
			try
			{
				var userToCourseLink = db.UsersToCourses
					.FirstOrDefault(uc => uc.CourseId == courseId && uc.UserId.Equals(userId));

				if(userToCourseLink != null)
				{
					var dbFile = filesFactory.CreateFile(formFile);
					dbFile.RelatedCourseId = courseId;
					dbFile.RelatedTopicId = topicId;
					db.Files.Attach(dbFile).State = EntityState.Added;
					db.SaveChanges();
					notificationManager.AddNotification("Plik pomyślnie dodany.", NotificationType.Success);
				}
				else
				{
					notificationManager
						.AddNotification("Podany kurs nie istanieje lub nie masz uprawnień do jego edycji.", true);
				}
			}
			catch(Exception ex)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void AddProjectFile(IFormFile formFile, int projectId, string userId)
		{
			try
			{
				var dbFile = db.ProjectFiles.FirstOrDefault(f => f.ProjectId == projectId);
				if(dbFile == null)
				{
					dbFile = mapper.Map<ProjectFile>(formFile);
					dbFile.ProjectId = projectId;
					db.ProjectFiles.Attach(dbFile).State = EntityState.Added;
				}
				else
				{
					mapper.Map<IFormFile, ProjectFile>(formFile, dbFile);
					dbFile.ProjectId = projectId;
					db.ProjectFiles.Attach(dbFile).State = EntityState.Modified;
				}
				db.SaveChanges();
			}
			catch (Exception)
			{

				throw;
			}
		}

		public void DeleteFile(int fileId, string userId)
		{
			try
			{
				var file = db.Files
					.Where(f => f.FileId == fileId)
					.Include(f => f.RelatedCourse.AssociatedUsers)
					.Include(f => f.RelatedTopic.Course.AssociatedUsers)
					.FirstOrDefault();

				if (file?.RelatedCourse?.AssociatedUsers?.FirstOrDefault(au => au.UserId.Equals(userId)) != null
					|| file?.RelatedTopic?.Course?.AssociatedUsers.FirstOrDefault(au => au.UserId.Equals(userId)) != null)
				{
					db.Files.Attach(file).State = EntityState.Deleted;
					db.SaveChanges();
					notificationManager.AddNotification("Plik usunięty pomyślnie.", NotificationType.Success);
				}
				else
				{
					notificationManager.AddNotification("Nie udało się usunąć pliku.", true);
				}
			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbReadError, true);
				throw;
			}
		}

		public void DeleteProjectFile(int projectId, string userId)
		{
			try
			{
				var file = db.ProjectFiles
					.FirstOrDefault(f => f.ProjectId == projectId);

				if (file != null)
				{
					db.ProjectFiles.Attach(file).State = EntityState.Deleted;
					db.SaveChanges();
					notificationManager.AddNotification("Plik usunięty pomyślnie.", NotificationType.Success);
				}
				else
				{
					notificationManager.AddNotification("Nie udało się usunąć pliku.", true);
				}
			}
			catch (Exception ex)
			{
				notificationManager.AddNotification(EventType.DbReadError, true);
				throw;
			}
		}

		public (byte[] file, string contentType) GetCourseFile(int fileId, int courseId, 
			string userId)
		{
			(byte[] file, string contentType) result = (null, null);
			try
			{
				var course = db.Courses
					.Include(c => c.AssociatedUsers)
					.Include(c => c.Files)
					.FirstOrDefault(c => c.CourseId == courseId);

				if(course == null)
				{
					notificationManager.AddNotification("Powiązany kurs nie istnieje", true);
				}
				else
				{
					var dbFile = course.Files.FirstOrDefault(f => f.FileId == fileId);

					if (dbFile == null) notificationManager
							.AddNotification("Plik nie istnieje.", true);
					else
					{
						result = (dbFile.Data, dbFile.ContentType);
						notificationManager
						 .AddNotification("Plik znaleziony pomyślnie.", NotificationType.Success);
					}

				}
			}
			catch(Exception ex)
			{
				notificationManager.AddNotification(EventType.DbReadError, true);
				throw;
			}
			return result;
		}

		public (byte[] file, string contentType) GetProjectFile(int projectId, string userId)
		{
			(byte[] file, string contentType) result = (null, null);
			try
			{
				var dbFile = db.ProjectFiles.FirstOrDefault(f => f.ProjectId == projectId);
				result = (dbFile.Data, dbFile.ContentType);
			}
			catch (Exception)
			{

				throw;
			}
			return result;
		}
	}
}
