﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Web.Factories;
using PlatformaELearningowa.Web.Models;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Topics;

namespace PlatformaELearningowa.Web.Services
{
	public class TopicsService : ITopicsService
	{
		private readonly ELearningContext db;
		private readonly ITopicsFactory topicsFactory;
		private readonly INotificationManager notificationManager;

		public TopicsService(ITopicsFactory topicsFactory,
			INotificationManager notificationManager,
			ELearningContext db)
		{
			this.db = db;
			this.topicsFactory = topicsFactory;
			this.notificationManager = notificationManager;
		}

		public void AddNewTopic(TopicWriteVM topic)
		{
			try
			{
				var existingTopics =  db.Topics
					.Where(t => t.CourseId == topic.CourseId)
					.ToList();
				var dbTopic = topicsFactory.CreateTopic(topic);
				dbTopic.TopicNumber = existingTopics.Count() + 1;
				dbTopic.TopicId = (existingTopics.Count > 0)? existingTopics.Max(t => t.TopicId) + 1 : 1;
				db.Entry<Topic>(dbTopic).State = EntityState.Added;
				db.SaveChanges();
				notificationManager.AddNotification("Temat poyślnie dodany.", NotificationType.Success);
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void ChangeTopicNumber(int courseId, int topicId, int newNumber)
		{
			try
			{
				// get related topics
				var topic1 = db.Topics
					.FirstOrDefault(t => t.CourseId == courseId && t.TopicId == topicId);
				var topic2 = db.Topics
					.FirstOrDefault(t => t.CourseId == courseId && t.TopicNumber == newNumber);
				if(topic1 == null || topic2 == null)
				{
					notificationManager
						.AddNotification("Nie udało się zmienić numeru zajęć.", true);
				}
				else
				{
					int temp = topic2.TopicNumber;
					topic2.TopicNumber = topic1.TopicNumber;
					topic1.TopicNumber = temp;
					db.Entry<Topic>(topic1).State = EntityState.Modified;
					db.Entry<Topic>(topic2).State = EntityState.Modified;
					db.SaveChanges();
					notificationManager
						.AddNotification("Numer zajęć zmieniony pomyślnie.", NotificationType.Success);
				}
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void DeleteTopic(int courseId, int topicId)
		{
			try
			{
				// Get course topics
				var courseTopics = db.Topics
					.Where(t => t.CourseId == courseId)
					.Include(t =>  t.Files)
					.ToList();

				// Find topic for deletion
				var toDelete = courseTopics.FirstOrDefault(t => t.TopicId == topicId);

				// Find all topics after deleted topic to downgrade their ordinal number
				var toModify = courseTopics.Where(t => t.TopicNumber > toDelete.TopicNumber);

				// Modify and set proper state for all entities
				db.Entry<Topic>(toDelete).State = EntityState.Deleted;

				foreach(var file in toDelete.Files)
				{
					db.Entry<File>(file).State = EntityState.Deleted;
				}

				foreach(var topic in toModify)
				{
					topic.TopicNumber--;
					db.Entry<Topic>(topic).State = EntityState.Modified;
				}

				db.SaveChanges();
				notificationManager.AddNotification("Temat pomyślnie usunięty.", NotificationType.Success);
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void EditExisitngTopic(TopicWriteVM topic)
		{
			try
			{
				var dbTopic = db.Topics.FirstOrDefault(t => t.TopicId == topic.TopicId && t.CourseId == topic.CourseId);
				dbTopic.Name = topic.Name;
				dbTopic.Description = topic.Description;
				db.Entry<Topic>(dbTopic).State = EntityState.Modified;
				db.SaveChanges();
			}
			catch (Exception)
			{

				throw;
			}
		}

		public IEnumerable<TopicSimpleVM> GetCourseTopics(int courseId)
		{
			IEnumerable<TopicSimpleVM> result = null;
			try
			{
				result = db.Topics.Where(t => t.CourseId == courseId)
					.ToList()
					.Select(t => topicsFactory.CreateSimpleViewModel(t));
			}
			catch (Exception)
			{

				throw;
			}
			return result;
		}

		public TopicDetailedVM GetTopicDetails(int courseId, int topicId)
		{
			TopicDetailedVM result = null;

			try
			{
				var dbTopic = db.Topics
					.Include(t => t.Files)
					.FirstOrDefault(t => t.CourseId == courseId && t.TopicId == topicId);
				result = topicsFactory.CreateDetailedViewModel(dbTopic);
			}
			catch (Exception)
			{

				throw;
			}

			return result;
		}

		public TopicWriteVM GetTopicForEdition(int courseId, int topicId)
		{
			TopicWriteVM result = null;
			try
			{
				var dbTopic = db.Topics
					.FirstOrDefault(t => t.CourseId == courseId && t.TopicId == topicId);

				result = topicsFactory.CreateWriteViewModel(dbTopic);
			}
			catch (Exception)
			{

				throw;
			}
			return result;
		}
	}
}
