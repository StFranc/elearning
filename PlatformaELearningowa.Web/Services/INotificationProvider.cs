﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Services
{
    public interface INotificationProvider
    {
		string GetNotificationForEvent(EventType eventType);
    }
}
