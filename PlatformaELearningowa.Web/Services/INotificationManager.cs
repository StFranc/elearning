﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Services
{
    public interface INotificationManager
    {
		bool CriticalErrorOccured { get; }
		void AddNotification(EventType eventType, NotificationType notificationType);
		void AddNotification(EventType eventType, bool criticalError);
		void AddNotification(string content, NotificationType notificationType);
		void AddNotification(string content, bool criticalError);
		(string content, NotificationType type) GetLastNotification();
	}
}
