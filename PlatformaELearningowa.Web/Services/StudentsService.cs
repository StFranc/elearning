﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Web.Factories;
using PlatformaELearningowa.Web.Models;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Students;
using PlatformaELearningowa.Web.Models.ViewModels.Groups;
using PlatformaELearningowa.Web.Models.ViewModels.Projects;

namespace PlatformaELearningowa.Web.Services
{
	public class StudentsService : IStudentsService
	{
		private readonly IMapper mapper;
		private readonly ELearningContext db;
		private readonly IStudentsFactory studentsFactory;
		private readonly UserManager<User> userManager;
		private readonly RoleManager<Role> roleManager;
		private readonly INotificationManager notificationManager;

		public StudentsService(IMapper mapper,
			ELearningContext db,
			IStudentsFactory studentsFactory,
			UserManager<User> userManager,
			RoleManager<Role> roleManager,
			INotificationManager notificationManager)
		{
			this.mapper = mapper;
			this.db = db;
			this.studentsFactory = studentsFactory;
			this.roleManager = roleManager;
			this.userManager = userManager;
			this.notificationManager = notificationManager;
		}

		public void DeleteStudent(string studentId)
		{
			try
			{
				var user = db.Users.FirstOrDefault(u => u.Id.Equals(studentId));
				if (user == null)
				{
					notificationManager.AddNotification("Student nie istnieje.", true);
				}

				else if (userManager.IsInRoleAsync(user, "Student").Result)
				{
					db.Entry<User>(user).State = EntityState.Deleted;
					db.SaveChanges();
					notificationManager.AddNotification("Student usunięty.", NotificationType.Success);
				}
				else
				{
					notificationManager.AddNotification("Użytkownik nie jest studentem.", true);
				}
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public void EditStudent(StudentWriteVM student)
		{
			try
			{
				var user = db.Users.FirstOrDefault(u => u.Id.Equals(student.Id));
				if(user == null)
				{
					notificationManager.AddNotification("Student nie istnieje.", true);
				}
				else if(userManager.IsInRoleAsync(user, "Student").Result)
				{
					mapper.Map<StudentWriteVM, User>(student, user);
					db.Entry<User>(user).State = EntityState.Modified;
					db.SaveChanges();
					notificationManager.AddNotification("Student zmodyfikowany.", NotificationType.Success);
				}
				else
				{
					notificationManager.AddNotification("Użytkownik nie jest studentem.", true);
				}
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}
		}

		public StudentWriteVM GetStudentForEdition(string studentId)
		{
			StudentWriteVM result = null;
			try
			{
				var user = db.Users.FirstOrDefault(u => u.Id.Equals(studentId));
				if (user == null)
				{
					notificationManager.AddNotification("Student nie istnieje.", true);
				}
				else if(userManager.IsInRoleAsync(user, "Student").Result)
				{
					var associatedGroups = db.UsersToGroups
						.Where(ug => ug.UserId.Equals(studentId))
						.Select(ug => ug.Group)
						.ToList();
					var availableGroups = db.Groups
						.ToList()
						.Except(associatedGroups);
					var assignedProjects = db.Projects
						.Include(p => p.Course)
						.Where(p => p.StudentId.Equals(studentId))
						.ToList();
					result = studentsFactory.CreateWriteViewModel(user);
					result.AssociatedGroups = mapper.Map<List<GroupSimpleVM>>(associatedGroups);
					result.AvailableGroups = mapper.Map<List<GroupSimpleVM>>(availableGroups);
					result.AssignedProjects = mapper.Map<List<ProjectSimpleVM>>(assignedProjects);
					notificationManager.AddNotification("Sukces.", NotificationType.Success);
				}
				else
				{
					notificationManager.AddNotification("Użytkownik nie jest studentem.", true);
				}
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbReadError, true);

				throw;
			}

			return result;
		}

		public IEnumerable<StudentSimpleVM> GetStudents(int pageNumber = 1, int pageSize = 20)
		{
			IEnumerable<StudentSimpleVM> result = null;
			try
			{
				var dbStudents = userManager.GetUsersInRoleAsync("Student").Result;
				result = dbStudents.Select(s => studentsFactory.CreateSimpleViewModel(s));
			}
			catch (Exception)
			{
				notificationManager.AddNotification(EventType.DbWriteError, true);
				throw;
			}

			return result;
		}
	}
}
