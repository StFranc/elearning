﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Students;

namespace PlatformaELearningowa.Web.Services
{
    public interface IStudentsService
    {
		IEnumerable<StudentSimpleVM> GetStudents(int pageNumber = 1, int pageSize = 20);
		StudentWriteVM GetStudentForEdition(string studentId);
		void EditStudent(StudentWriteVM student);
		void DeleteStudent(string studentId);
	}
}
