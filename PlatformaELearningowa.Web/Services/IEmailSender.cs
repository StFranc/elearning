﻿using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Services
{
	public interface IEmailSender
	{
		Task SendEmailAsync(string email, string subject, string message);
		Task SendEmailConfirmationAsync(string email, string link);
	}
}
