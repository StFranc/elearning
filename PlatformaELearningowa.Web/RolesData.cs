﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using PlatformaELearningowa.Web.Models;
using PlatformaELearningowa.Web.Models.Entities;

namespace PlatformaELearningowa.Web
{
	public static class RolesData
	{
		private static readonly string[] Roles = new string[] { "Administrator", "Teacher", "Student" };

		public static async Task SeedRoles(IServiceProvider serviceProvider)
		{
			using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
			{
				var dbContext = serviceScope.ServiceProvider.GetService<ELearningContext>();

				var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<Role>>();
				var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<User>>();
				if ((dbContext.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists())
				{ 
					if (!dbContext.UserRoles.Any())
					{

						foreach (var role in Roles)
						{
							if (!await roleManager.RoleExistsAsync(role))
							{
								await roleManager.CreateAsync(new Role { Name = role });
							}
						}
					}
					var teacher = await userManager.FindByEmailAsync("teacher@mail.com");
					if (teacher == null)
					{
						await userManager.CreateAsync(new User {
							UserName = "teacher@mail.com",
							Email = "teacher@mail.com" }, "Qwerty.123");
						teacher = await userManager.FindByEmailAsync("teacher@mail.com");
					}
					if(!await userManager.IsInRoleAsync(teacher, "Teacher"))
					{
						await userManager.AddToRoleAsync(teacher, "Teacher");
					}
				}
			}
		}
	}
}
