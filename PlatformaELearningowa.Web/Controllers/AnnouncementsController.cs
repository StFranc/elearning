﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Web.Models;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Announcements;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Controllers
{
	[Authorize(Roles = "Teacher")]
    public class AnnouncementsController : Controller
    {
		private readonly IMapper mapper;
		private readonly ELearningContext db;

		public AnnouncementsController(ELearningContext db,
			IMapper mapper)
		{
			this.db = db;
			this.mapper = mapper;
		}

        public IActionResult Index()
		{
			IActionResult result = null;
			try
			{
				var dbAnnouncements = db.Announcements
					.Include(a => a.Course)
					.ToList();
				result = View(dbAnnouncements);
			}
			catch (Exception)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				throw;
			}
			return result;
		}

		public IActionResult Form(int? announcementId)
		{
			IActionResult result = null;
			try
			{
				var courses = mapper.Map<IEnumerable<CourseSimpleVM>>(db.Courses.ToList());
				AnnouncementWriteVM announcement = null;
				if(announcementId != null)
				{
					var dbAnnouncement = db.Announcements.FirstOrDefault(a => a.AnnouncementId == announcementId);
					announcement = mapper.Map<AnnouncementWriteVM>(dbAnnouncement);

				}
				else
				{
					announcement = new AnnouncementWriteVM();
				}
				announcement.AvailableCourses = courses;
				result = View(announcement);
			}
			catch (Exception)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				throw;
			}
			return result;
		}

		public IActionResult Write(AnnouncementWriteVM announcement)
		{
			IActionResult result = null;
			try
			{
				if(announcement.AnnouncementId != null)
				{
					var dbAnnouncement = db.Announcements.FirstOrDefault(a => a.AnnouncementId == announcement.AnnouncementId);
					dbAnnouncement.Content = announcement.Content;
					dbAnnouncement.CourseId = announcement.CourseId;
					dbAnnouncement.Title = announcement.Title;
					db.Attach(dbAnnouncement).State = EntityState.Modified;
				}
				else
				{
					var dbAnnouncement = mapper.Map<Announcement>(announcement);
					db.Attach(dbAnnouncement).State = EntityState.Added;
				}
				db.SaveChanges();
				result = RedirectToAction("Index");
			}
			catch (Exception)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				throw;
			}
			return result;
		}

		public IActionResult Delete(int announcementId)
		{
			IActionResult result = null;
			try
			{
				var toDelete = db.Announcements.FirstOrDefault(a => a.AnnouncementId == announcementId);
				db.Attach(toDelete).State = EntityState.Deleted;
				db.SaveChanges();
				result = Content("Ogłoszenie usunięte pomyslnie.");
			}
			catch (Exception)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				result = Content("Nie udało się usunąć ogłoszenia.");
				throw;
			}
			return result;
		}
	}
}