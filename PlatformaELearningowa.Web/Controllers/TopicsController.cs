﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PlatformaELearningowa.Web.Factories;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Topics;
using PlatformaELearningowa.Web.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PlatformaELearningowa.Web.Controllers
{
	[Authorize]
    public class TopicsController : Controller
    {
		private readonly ITopicsService topicsService;
		private readonly INotificationManager notificationManager;
		private readonly UserManager<User> userManager;
		private readonly ITopicsFactory topicsFactory;

		public TopicsController(ITopicsService topicsService,
			INotificationManager notificationManager,
			UserManager<User> userManager,
			ITopicsFactory topicsFactory)
		{

			this.topicsService = topicsService;
			this.notificationManager = notificationManager;
			this.topicsFactory = topicsFactory;
			this.userManager = userManager;
		}

        // GET: /<controller>/
		[HttpGet]
        public IActionResult Index(int courseId)
        {
			var topics = topicsService.GetCourseTopics(courseId);
            return View(topics);
        }

		[HttpGet]
		public IActionResult Details(int courseId, int topicId)
		{
			var topic = topicsService.GetTopicDetails(courseId, topicId);
			return View(topic);
		}

		[HttpGet]
		public IActionResult Form(int courseId, int? topicId)
		{
			TopicWriteVM topicVM = null;
			if(topicId != null)
			{
				topicVM = topicsService.GetTopicForEdition(courseId, (int)topicId);
			}
			else
			{
				topicVM = topicsFactory.CreateWriteViewModel(courseId);
				topicVM.CourseId = courseId;
			}
			return View(topicVM);
		}

		[HttpPost]
		public IActionResult Create(TopicWriteVM topic)
		{

			IActionResult result = null;
			if (ModelState.IsValid)
			{
				var userId = userManager.GetUserId(User);
				topicsService.AddNewTopic(topic);
				result = RedirectToAction("Details","Courses", new { topic.CourseId });
			}
			else
			{
				result = View("Form", topic);
			}
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Update(TopicWriteVM topic)
		{
			IActionResult result = null;
			if (ModelState.IsValid)
			{
				topicsService.EditExisitngTopic(topic);
				result = RedirectToAction("Details", "Courses", new { topic.CourseId });
			}
			else
			{
				result = View("Form", topic);
			}
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Delete(int courseId, int topicId)
		{
			IActionResult result = null;
			var userId = userManager.GetUserId(User);
			topicsService.DeleteTopic(courseId, topicId);
			(var notificationContent, var notificationType) = notificationManager.GetLastNotification();
			if (notificationType == NotificationType.Error || notificationManager.CriticalErrorOccured)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
			}
			result = Content(notificationContent);

			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult ChangeTopicNumber(int courseId, int topicId, int newNumber)
		{
			IActionResult result = null;
			topicsService.ChangeTopicNumber(courseId, topicId, newNumber);
			(string content, NotificationType type) = notificationManager.GetLastNotification();

			if(type == NotificationType.Error || notificationManager.CriticalErrorOccured)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
			}
			result = Content(content);

			return result;
		}
	}
}
