﻿using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PlatformaELearningowa.Web.Services;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Factories;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Controllers
{
	[Authorize]
    public class CoursesController : Controller
    {
		private readonly ICoursesService coursesService;
		private readonly INotificationManager notificationManager;
		private readonly UserManager<User> userManager;
		private readonly ICoursesFactory coursesFactory;

		public CoursesController(
			ICoursesService coursesService,
			INotificationManager notificationManager,
			UserManager<User> userManager,
			ICoursesFactory coursesFactory)
		{
			this.coursesService = coursesService;
			this.notificationManager = notificationManager;
			this.userManager = userManager;
			this.coursesFactory = coursesFactory;
		}

		[HttpGet]
        public IActionResult Index(bool onlyAttended = false, int pageNumber = 1, int pageSize = 20)
        {
			IEnumerable<CourseSimpleVM> courses = null;
			if (onlyAttended)
			{
				var userId = userManager.GetUserId(User);
				courses = coursesService.GetAttendedCourses(pageNumber, pageSize, userId); 
			}
			else
			{
				courses = coursesService.GetCourses(pageNumber, pageSize);
			}
            return View(courses);
        }

		[HttpGet]
		public IActionResult Details(int courseId)
		{
			var userId = userManager.GetUserId(User);
			CourseDetailedVM course = coursesService.GetCourse(courseId, userId);
			IActionResult result = null;

			if(course != null)
			{
				result = View(course);
			}
			else
			{
				var enrollmentVM = coursesFactory.CreateCourseEnrollmentVM(courseId, null);
				result = View("EnrollmentForm", enrollmentVM);
			}
			return result;
		}

		[HttpGet]
		public IActionResult GetForm(int? courseId)
		{
			IActionResult result = null;

			if(courseId != null)
			{
				var userId = userManager.GetUserId(User);
				var course = coursesService.GetCourseForEditing((int)courseId, userId);

				if(course == null)
				{
					var enrollmentVM = coursesFactory
						.CreateCourseEnrollmentVM((int)courseId, null);
					result = View("EnrollmentForm", enrollmentVM);
				}
				else
				{
					result = View("Form", course);
				}
			}
			else
			{
				result = View("Form", null);
			}

			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Create(CourseWriteVM course)
		{
			IActionResult result = null;
			if (ModelState.IsValid)
			{
				var userId = userManager.GetUserId(User);
				course.CreatorId = userId;
				coursesService.AddCourse(course);
				result = RedirectToAction("Index");
			}
			else
			{
				result = View("Form", course);
			}
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Update(CourseWriteVM course)
		{
			IActionResult result = null;
			if (ModelState.IsValid)
			{
				coursesService.EditCourse(course);
				result = RedirectToAction("Index");
			}
			else
			{
				result = View("Form", course);
			}
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Delete(int courseId)
		{
			var userId = userManager.GetUserId(User);
			coursesService.RemoveCourse(courseId, userId);
			(var notificationContent, var notificationType) = notificationManager.GetLastNotification();
			if (notificationType == NotificationType.Error || notificationManager.CriticalErrorOccured)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
			}

			return RedirectToAction("Index");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Enroll(CourseEnrollmentVM model)
		{
			if (!ModelState.IsValid)
			{
				return View("EnrollmentForm", model);
			}

			IActionResult result = null;
			var userId = userManager.GetUserId(User);
			coursesService.EnrollToCourse(model.EnrollmentKey, userId, model.CourseId);
			(var notificationContent, var notificationType) = notificationManager.GetLastNotification();

			if (notificationType == NotificationType.Error || notificationManager.CriticalErrorOccured)
			{
				ModelState.AddModelError("EnrollmentKey", notificationContent);
				result = View("EnrollmentForm", model);
			}
			else
			{
				result = RedirectToAction("Details", new { courseId = model.CourseId });
			}

			return result;
		}
	}
}