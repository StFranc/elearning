﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PlatformaELearningowa.Web.Models.ViewModels.Students;
using PlatformaELearningowa.Web.Services;

namespace PlatformaELearningowa.Web.Controllers
{
	[Authorize(Roles = "Teacher")]
    public class StudentsController : Controller
    {
		private readonly INotificationManager notificationManager;
		private readonly IStudentsService studentsService;

		public StudentsController(INotificationManager notificationManager,
			IStudentsService studentsService)
		{
			this.notificationManager = notificationManager;
			this.studentsService = studentsService;
		}

        public IActionResult Index()
        {
			IActionResult result = null;
			var students = studentsService.GetStudents();
			result = View(students);
            return result;
        }

		public IActionResult Form(string studentId)
		{
			IActionResult result = null;

			var student = studentsService.GetStudentForEdition(studentId);
			result = View(student);

			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Update(StudentWriteVM model)
		{
			IActionResult result = null;
			if (ModelState.IsValid)
			{
				studentsService.EditStudent(model);

				(var content, var type) = notificationManager.GetLastNotification();
				ViewData["Success"] = 
					!notificationManager.CriticalErrorOccured && type != NotificationType.Error;
				ViewData["Notification"] = content;
			}
			result = View("Form", model);
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Delete(string studentId)
		{
			IActionResult result = null;
			if (ModelState.IsValid)
			{
				studentsService.DeleteStudent(studentId);

				(var content, var type) = notificationManager.GetLastNotification();
				if(type == NotificationType.Error || notificationManager.CriticalErrorOccured)
				{
					Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				}
				result = Content(content);
			}
			return result;
		}

	}
}