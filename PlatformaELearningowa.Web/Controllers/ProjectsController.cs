﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PlatformaELearningowa.Web.Models;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Projects;
using System.Net;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Controllers
{
	[Authorize]
    public class ProjectsController : Controller
    {
		private readonly ELearningContext db;
		private readonly IMapper mapper;
		private readonly UserManager<User> userManager;

		public ProjectsController(IMapper mapper,
			ELearningContext db,
			UserManager<User> userManager)
		{
			this.db = db;
			this.mapper = mapper;
			this.userManager = userManager;
		}

		[Authorize(Roles = "Student")]
		public IActionResult Index()
		{
			IActionResult result = null;
			try
			{
				var userId = userManager.GetUserId(User);
				var dbProjects = db.Projects
					.Where(p => p.StudentId.Equals(userId))
					.Include(p => p.Course)
					.ToList();
				var projects = mapper.Map<IEnumerable<ProjectSimpleVM>>(dbProjects);
				result = View(projects);
			}
			catch (Exception)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				throw;
			}
			return result;
		}

		[Authorize(Roles = "Student")]
		public IActionResult StudentDetails(int projectId)
		{
			IActionResult result = null;
			try
			{
				var dbProject = db.Projects
					.Include(p => p.File)
					.Include(p => p.Course)
					.First(p => p.ProjectId == (int)projectId);
				var project = mapper.Map<ProjectStudentVM>(dbProject);
				result = View(project);
			}
			catch (Exception)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				throw;
			}
			return result;
		}

		[Authorize(Roles = "Teacher")]
		public IActionResult TeacherDetails(int? projectId = null, string studentId = null)
		{
			IActionResult result = null;
			try
			{
				var availabeDbCourses = db.Courses;
				if(projectId != null)
				{
					ViewBag.NewProject = false;
					var dbProject = db.Projects
						.Include(p => p.File)
						.First(p => p.ProjectId == (int)projectId);
					var project = mapper.Map<ProjectTeacherVM>(dbProject);
					project.AvailableCourses = mapper.Map<List<CourseSimpleVM>>(availabeDbCourses.ToList());
					result = View(project);
				}
				else if (!String.IsNullOrWhiteSpace(studentId))
				{
					ViewBag.NewProject = true;
					var newProject = new ProjectTeacherVM {
						StudentId = studentId,
						AvailableCourses = mapper.Map<List<CourseSimpleVM>>(availabeDbCourses.ToList())
					};
					result = View(newProject);
				}
				else
				{
					Response.StatusCode = (int)HttpStatusCode.BadRequest;
				}
			}
			catch (Exception)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				throw;
			}
			return result;
		}

		[Authorize(Roles = "Teacher")]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Write(ProjectTeacherVM model)
		{
			IActionResult result = null;
			try
			{
				if(model.ProjectId == null)
				{
					var dbProject = mapper.Map<Project>(model);
					db.Entry<Project>(dbProject).State = EntityState.Added;
				}
				else
				{
					var dbProject = db.Projects
						.FirstOrDefault(p => p.ProjectId == model.ProjectId);
					mapper.Map<ProjectTeacherVM, Project>(model, dbProject);
					db.Entry<Project>(dbProject).State = EntityState.Modified;
				}
				db.SaveChanges();
				result = RedirectToAction("Form", "Students", new { studentId = model.StudentId });
			}
			catch (Exception)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				throw;
			}
			return result;
		}

		[Authorize(Roles = "Teacher")]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Delete(int projectId)
		{
			IActionResult result = null;
			try
			{
				var toDelete = db.Projects.FirstOrDefault(p => p.ProjectId == projectId);
				db.Entry<Project>(toDelete).State = EntityState.Deleted;
				db.SaveChanges();
				result = Content("Projekt usunięty pomyślnie.");
			}
			catch (Exception)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				result = Content("Usuwanie projektu nie powiodło się.");
				throw;
			}
			return result;
		}
	}
}