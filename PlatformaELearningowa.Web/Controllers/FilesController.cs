﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Services;
using MimeTypes.Core;
using System.ComponentModel.DataAnnotations;
using System.Net;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;
using PlatformaELearningowa.Web.Models.ViewModels.Projects;

namespace PlatformaELearningowa.Web.Controllers
{
	[Authorize]
	public class FilesController : Controller
	{
		private readonly IFilesService filesService;
		private readonly UserManager<User> userManager;
		private readonly INotificationManager notificationManager;

		public FilesController(
			IFilesService filesService,
			UserManager<User> userManager,
			INotificationManager notificationManager)
		{
			this.filesService = filesService;
			this.userManager = userManager;
			this.notificationManager = notificationManager;
		}

		[HttpGet]
		public IActionResult CourseFileDownload(int fileId, int courseId)
		{
			IActionResult result = null;

			var userId = userManager.GetUserId(User);

			(byte[] file, string contentType)= filesService.GetCourseFile(fileId, courseId, userId);

			result = File(file, contentType, String
				.Format("{0}{1}","download", MimeTypeMap.GetExtension(contentType)));

			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult CourseFileUpload(CourseFileUploadVM uploadData)
		{
			if (!ModelState.IsValid)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				var errorMessages = ModelState.Values.SelectMany(m => m.Errors.Select(b => b.ErrorMessage));
				return Content(errorMessages.First());
			}
			var userId = userManager.GetUserId(User);

			filesService.AddCourseFile(uploadData.File, uploadData.CourseId, userId, uploadData.TopicId);

			return RedirectToAction("Details", "Courses", new { uploadData.CourseId });
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult FileDelete(int fileId)
		{
			var userId = userManager.GetUserId(User);

			filesService.DeleteFile(fileId, userId);
			(var content, var type) = notificationManager.GetLastNotification();

			if(notificationManager.CriticalErrorOccured || type == NotificationType.Error)
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;

			return Content(content);
		}



		[HttpGet]
		public IActionResult ProjectFileDownload(int projectId)
		{
			IActionResult result = null;

			var userId = userManager.GetUserId(User);

			(byte[] file, string contentType) = filesService.GetProjectFile(projectId, userId);

			result = File(file, contentType, String
				.Format("{0}{1}", "download", MimeTypeMap.GetExtension(contentType)));

			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult ProjectFileUpload(ProjectFileUploadVM uploadData)
		{
			if (!ModelState.IsValid)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				var errorMessages = ModelState.Values.SelectMany(m => m.Errors.Select(b => b.ErrorMessage));
				return Content(errorMessages.First());
			}
			var userId = userManager.GetUserId(User);

			filesService.AddProjectFile(uploadData.File, uploadData.ProjectId, userId);

			return RedirectToAction("StudentDetails", "Projects", new { uploadData.ProjectId });
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult ProjectFileDelete(int projectId)
		{
			var userId = userManager.GetUserId(User);

			filesService.DeleteProjectFile(projectId, userId);
			(var content, var type) = notificationManager.GetLastNotification();

			if (notificationManager.CriticalErrorOccured || type == NotificationType.Error)
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;

			return Content(content);
		}
	}
}