﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PlatformaELearningowa.Web.Models;
using PlatformaELearningowa.Web.Models.ViewModels.Common;

namespace PlatformaELearningowa.Web.Controllers
{
	[Authorize]
    public class HomeController : Controller
    {
		private readonly ELearningContext db;
		public HomeController(ELearningContext db)
		{
			this.db = db;
		}

        public IActionResult Index()
        {
			try
			{
				var announcements = db.Announcements.Where(a => a.CourseId == null).ToList();
				return View(announcements);
			}
			catch (System.Exception)
			{

				throw;
			}
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
