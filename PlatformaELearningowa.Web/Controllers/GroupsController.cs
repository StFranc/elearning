﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Groups;
using PlatformaELearningowa.Web.Services;

namespace PlatformaELearningowa.Web.Controllers
{
	[Authorize(Roles = "Teacher")]
    public class GroupsController : Controller
    {
		private readonly IGroupsService groupsService;
		private readonly INotificationManager notificationManager;
		private readonly UserManager<User> userManager;

		public GroupsController(IGroupsService groupsService,
			INotificationManager notificationManager,
			UserManager<User> userManager)
		{
			this.groupsService = groupsService;
			this.notificationManager = notificationManager;
			this.userManager = userManager;
		}

		[HttpGet]
		public IActionResult Index()
		{
			IActionResult result = null;
			var userId = userManager.GetUserId(User);
			var groups = groupsService.GetGroups(userId);
			result = View(groups);
			return result;
		}

		[HttpGet]
		public IActionResult Form(int? groupId)
		{
			IActionResult result = null;
			if(groupId != null)
			{
				var userId = userManager.GetUserId(User);
				var viewModel = groupsService.GetGroupForEdition((int)groupId, userId);
				result = View(viewModel);
			}
			else
			{
				result = View();
			}
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Write(GroupWriteVM model)
		{
			IActionResult result = null;
			if (ModelState.IsValid)
			{
				var userId = userManager.GetUserId(User);
				if (model.GroupId == 0)
				{
					groupsService.AddGroup(model, userId);
				}
				else
				{
					groupsService.ModifyGroup(model, userId);
				}
				result = RedirectToAction("Index");
			}

			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Delete(int groupId)
		{
			IActionResult result = null;
			var userId = userManager.GetUserId(User);
			groupsService.DeleteGroup(groupId,userId);
			(string content, NotificationType type) = notificationManager.GetLastNotification();
			if(notificationManager.CriticalErrorOccured || type == NotificationType.Error)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
			}
			result = Content(content);
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult AssociateUser(int groupId, string userId)
		{
			IActionResult result = null;
			groupsService.AssociateUser(groupId, userId);
			(string content, NotificationType type) = notificationManager.GetLastNotification();
			if (notificationManager.CriticalErrorOccured || type == NotificationType.Error)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
			}
			result = RedirectToAction("Form","Students", new { studentId = userId });
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult UnassociateUser(int groupId, string userId)
		{
			IActionResult result = null;
			groupsService.UnassociateUser(groupId, userId);
			(string content, NotificationType type) = notificationManager.GetLastNotification();
			if (notificationManager.CriticalErrorOccured || type == NotificationType.Error)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
			}
			result = RedirectToAction("Form", "Students", new { studentId = userId });
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult AssociateCourse(int groupId, int courseId)
		{
			IActionResult result = null;
			groupsService.AssociateCourse(groupId, courseId);
			(string content, NotificationType type) = notificationManager.GetLastNotification();
			if (notificationManager.CriticalErrorOccured || type == NotificationType.Error)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
			}
			result = RedirectToAction("Form", "Groups", new { groupId = groupId });
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult UnassociateCourse(int groupId, int courseId)
		{
			IActionResult result = null;
			groupsService.UnassociateCourse(groupId, courseId);
			(string content, NotificationType type) = notificationManager.GetLastNotification();
			if (notificationManager.CriticalErrorOccured || type == NotificationType.Error)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
			}
			result = RedirectToAction("Form", "Groups", new { groupId = groupId });
			return result;
		}
	}
}