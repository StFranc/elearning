﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class GroupToCourse
    {
		public int GroupId { get; set; }
		public virtual Group Group { get; set; }

		public int CourseId { get; set; }
		public virtual Course Course { get; set; }
	}
}
