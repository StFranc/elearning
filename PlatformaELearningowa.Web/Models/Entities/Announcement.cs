﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class Announcement
    {
		public int AnnouncementId { get; set; }

		public string Title { get; set; }

		public string Content { get; set; }

		public int? CourseId { get; set; }
		public virtual Course Course { get; set; }
	}
}
