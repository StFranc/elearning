﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class UserToGroup
    {
		public string UserId { get; set; }
		public virtual User User { get; set; }

		public int GroupId { get; set; }
		public virtual Group Group { get; set; }
	}
}
