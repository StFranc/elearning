﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class Role : IdentityRole
    {
		public virtual ICollection<IdentityUserRole<string>> Users { get; } = new List<IdentityUserRole<string>>();
		/*
		public Role() { }

		public Role(string roleName) : base()
		{
			Name = roleName;
		}
		*/
	}
}
