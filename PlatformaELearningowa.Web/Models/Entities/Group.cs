﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class Group
    {
		public int GroupId { get; set; }

		public string Name { get; set; }

		public virtual ICollection<UserToGroup> AssociatedUsers { get; set; }

		public virtual ICollection<GroupToCourse> AssociatedCourses { get; set; }
	}
}
