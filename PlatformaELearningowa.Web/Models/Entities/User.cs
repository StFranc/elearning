﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class User : IdentityUser
	{
		public virtual ICollection<UserToCourse> AssociatedCourses { get; set; }

		public virtual ICollection<UserToGroup> AssociatedGroups { get; set; }
		
		public virtual ICollection<IdentityUserRole<string>> Roles { get; } = new List<IdentityUserRole<string>>();

		public virtual ICollection<Project> AssignedProjects { get; set; }
	}
}
