﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class Project
    {
		public int ProjectId { get; set; }

		public int CourseId { get; set; }
		public virtual Course Course { get; set; }

		public string TeacherId { get; set; }
		public virtual User Teacher { get; set; }

		public string StudentId { get; set; }
		public virtual User Student { get; set; }

		public string Title { get; set; }

		public string Description { get; set; }

		public string PrivateTeacherComment { get; set; }
		public string PublicTeacherComment { get; set; }

		public virtual ProjectFile File { get; set; }
	}
}
