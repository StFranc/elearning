﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class ProjectFile
    {
		public int ProjectId { get; set; }
		public virtual Project Project { get; set; }

		public string Name { get; set; }

		public string ContentType { get; set; }

		public byte[] Data { get; set; } 
	}
}
