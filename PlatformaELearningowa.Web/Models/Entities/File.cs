﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class File
    {
		public int FileId { get; set; }

		public int? RelatedCourseId { get; set; }
		public virtual Course RelatedCourse { get; set; }

		public int? RelatedTopicId { get; set; }
		public virtual Topic RelatedTopic { get; set; }

		public string Name { get; set; }

		public string ContentType { get; set; }

		public string Description { get; set; }

		public byte[] Data { get; set; }
	}
}
