﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class UserToCourseMap : IEntityTypeConfiguration<UserToCourse>
	{
		public void Configure(EntityTypeBuilder<UserToCourse> builder)
		{
			builder.HasKey(uc => new { uc.CourseId, uc.UserId });
		}
	}
}
