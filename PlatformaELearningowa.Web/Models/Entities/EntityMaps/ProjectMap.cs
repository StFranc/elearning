﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class ProjectMap : IEntityTypeConfiguration<Project>
	{
		public void Configure(EntityTypeBuilder<Project> builder)
		{
			// TODO: write ORM mapping
			builder.HasKey(x => x.ProjectId);
			builder.HasOne(x => x.Student)
				.WithMany(x => x.AssignedProjects)
				.HasForeignKey(x => x.StudentId)
				.IsRequired();
			builder.HasOne(x => x.Teacher)
				.WithMany()
				.HasForeignKey(x => x.TeacherId)
				.OnDelete(DeleteBehavior.ClientSetNull);
			builder.HasOne(x => x.Course)
				.WithMany()
				.HasForeignKey(x => x.CourseId)
				.IsRequired();
			builder.HasOne(x => x.File)
				.WithOne(x => x.Project)
				.HasForeignKey<ProjectFile>();
		}
	}
}
