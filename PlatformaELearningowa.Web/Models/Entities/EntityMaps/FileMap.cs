﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class FileMap : IEntityTypeConfiguration<File>
	{
		public void Configure(EntityTypeBuilder<File> builder)
		{
			builder.HasKey(f => f.FileId);
			builder.Property(f => f.Name).IsRequired();
			builder.Property(f => f.Description);
			builder
				.HasOne(f => f.RelatedCourse)
				.WithMany(c => c.Files)
				.HasForeignKey(f => f.RelatedCourseId);
			builder
				.HasOne(f => f.RelatedTopic)
				.WithMany(t => t.Files)
				.HasForeignKey(f => new { f.RelatedTopicId, f.RelatedCourseId });
		}
	}
}
