﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class ProjectFileMap : IEntityTypeConfiguration<ProjectFile>
	{
		public void Configure(EntityTypeBuilder<ProjectFile> builder)
		{
			builder.HasKey(x => x.ProjectId);
			builder.HasOne(x => x.Project)
				.WithOne(x => x.File)
				.HasForeignKey<ProjectFile>()
				.IsRequired();
		}
	}
}
