﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class UserToGroupMap : IEntityTypeConfiguration<UserToGroup>
	{
		public void Configure(EntityTypeBuilder<UserToGroup> builder)
		{
			builder.HasKey(ug => new { ug.UserId, ug.GroupId });
			builder.HasOne(ug => ug.User)
				.WithMany(u => u.AssociatedGroups)
				.HasForeignKey(ug => ug.UserId);
			builder.HasOne(ug => ug.Group)
				.WithMany(g => g.AssociatedUsers)
				.HasForeignKey(ug => ug.GroupId);
		}
	}
}
