﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class GroupToCourseMap : IEntityTypeConfiguration<GroupToCourse>
	{
		public void Configure(EntityTypeBuilder<GroupToCourse> builder)
		{
			builder.HasKey(x => new { x.CourseId, x.GroupId });
			builder.HasOne(x => x.Course)
				.WithMany()
				.HasForeignKey(x => x.CourseId)
				.IsRequired();
			builder.HasOne(x => x.Group)
				.WithMany(g => g.AssociatedCourses)
				.HasForeignKey(x => x.GroupId)
				.IsRequired();

		}
	}
}
