﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class AnnouncementMap : IEntityTypeConfiguration<Announcement>
	{
		public void Configure(EntityTypeBuilder<Announcement> builder)
		{
			builder.HasKey(x => x.AnnouncementId);
			builder.HasOne(x => x.Course)
				.WithMany(x => x.Announcements)
				.HasForeignKey(x => x.CourseId);
		}
	}
}
