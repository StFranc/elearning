﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class CourseMap : IEntityTypeConfiguration<Course>
	{
		public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Course> builder)
		{
			builder.HasKey(c => c.CourseId);
			builder.Property(c => c.Name).IsRequired();
			builder.Property(c => c.Description).IsRequired();
			builder
				.HasMany(c => c.AssociatedUsers)
				.WithOne(uc => uc.Course)
				.HasForeignKey(uc => uc.CourseId)
				.IsRequired();
			builder
				.HasMany(c => c.Topics)
				.WithOne(t => t.Course)
				.OnDelete(DeleteBehavior.Cascade);
			builder
				.HasMany(c => c.Files)
				.WithOne(f => f.RelatedCourse)
				.HasForeignKey(f => f.RelatedCourseId)
				.OnDelete(DeleteBehavior.Cascade);
		}
	}
}
