﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class TopicMap : IEntityTypeConfiguration<Topic>
	{
		public void Configure(EntityTypeBuilder<Topic> builder)
		{
			builder.HasKey(t => new { t.CourseId, t.TopicId } );
			builder.Property(t => t.Name).IsRequired();
			builder.Property(t => t.Description).IsRequired();
			builder
				.HasOne(t => t.Course)
				.WithMany(c => c.Topics)
				.HasForeignKey(t => t.CourseId)
				.IsRequired();
			builder.HasMany(t => t.Files)
				.WithOne(f => f.RelatedTopic)
				.HasForeignKey(f => new { f.RelatedCourseId, f.RelatedTopicId })
				.OnDelete(DeleteBehavior.ClientSetNull);
		}
	}
}
