﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class RoleMap : IEntityTypeConfiguration<Role>
	{
		public void Configure(EntityTypeBuilder<Role> builder)
		{
			builder.HasKey(r => r.Id);
			builder.Property(r => r.Name).IsRequired();
			builder.HasMany(r => r.Users)
				.WithOne()
				.HasForeignKey(e => e.RoleId)
				.IsRequired();
		}
	}
}
