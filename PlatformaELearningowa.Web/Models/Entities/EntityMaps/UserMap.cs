﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PlatformaELearningowa.Web.Models.Entities.EntityMaps
{
	public class UserMap : IEntityTypeConfiguration<User>
	{
		public void Configure(EntityTypeBuilder<User> builder)
		{
			builder.HasKey(u => u.Id);
			builder
				.HasMany(u => u.AssociatedCourses)
				.WithOne(uc => uc.User)
				.HasForeignKey(uc => uc.UserId)
				.IsRequired();
			builder
				.HasMany(u => u.Roles)
				.WithOne()
				.HasForeignKey(r => r.UserId)
				.IsRequired();

		}
	}
}
