﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class Course
    {
		public int CourseId { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public string EnrollmentKey { get; set; }

		public virtual ICollection<Topic> Topics { get; set; }

		public virtual ICollection<File> Files { get; set; }

		public virtual ICollection<UserToCourse> AssociatedUsers { get; set; }

		public virtual ICollection<Announcement> Announcements { get; set; }
	}
}
