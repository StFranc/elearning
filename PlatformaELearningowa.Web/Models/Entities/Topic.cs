﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.Entities
{
    public class Topic
    {
		public int CourseId { get; set; }
		public virtual Course Course { get; set; }

		public int TopicId { get; set; }

		public int TopicNumber { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public virtual ICollection<File> Files { get; set; }
	}
}
