﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Common
{
    public class FileVM
	{
		public int FileId { get; set; }
		public string Name { get; set; }
		public string FileExtension { get; set; }
		public string Description { get; set; }
	}
}
