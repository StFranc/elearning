﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Announcements
{
    public class AnnouncementReadVM
	{
		public int AnnouncementId { get; set; }

		public string Title { get; set; }

		public string Content { get; set; }
	}
}
