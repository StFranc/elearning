﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Models.ViewModels.Announcements
{
    public class AnnouncementWriteVM
	{
		public int? AnnouncementId { get; set; }

		[Required(ErrorMessage = "Pole wymagane!")]
		[Display(Name = "Tytuł")]
		public string Title { get; set; }

		[Required(ErrorMessage = "Pole wymagane!")]
		[DataType(DataType.MultilineText)]
		[Display(Name = "Zawartość")]
		public string Content { get; set; }
		
		[Display(Name = "Powiązany Przedmiot")]
		public int? CourseId { get; set; }

		public IEnumerable<CourseSimpleVM> AvailableCourses { get; set; }
	}
}
