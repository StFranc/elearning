﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PlatformaELearningowa.Web.Models.ViewModels.Topics
{
    public class TopicWriteVM
    {
		public int CourseId { get; set; }

		public int? TopicId { get; set; }

		[Required(ErrorMessage = "Pole wymagane.")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Pole wymagane.")]
		public string Description { get; set; }
	}
}
