﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Topics
{
    public class TopicSimpleVM
	{
		public int CourseId { get; set; }

		public int? TopicId { get; set; }

		public int TopicNumber { get; set; }

		public string Name { get; set; }
	}
}
