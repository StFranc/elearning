﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Common;

namespace PlatformaELearningowa.Web.Models.ViewModels.Topics
{
    public class TopicDetailedVM
	{
		public int CourseId { get; set; }

		public int TopicId { get; set; }

		public int TopicNumber { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public IEnumerable<FileVM> Files { get; set; }
	}
}
