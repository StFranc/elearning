﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Account
{
    public class LoginViewModel
	{
		[Required(ErrorMessage = "Pole wymagane.")]
		[EmailAddress(ErrorMessage = "Podaj prawidłowy adres email.")]
		public string Email { get; set; }

		[Required(ErrorMessage = "Pole wymagane.")]
		[DataType(DataType.Password)]
		[Display(Name = "Hasło")]
		public string Password { get; set; }

        [Display(Name = "Zapamiętaj mnie.")]
        public bool RememberMe { get; set; }
    }
}
