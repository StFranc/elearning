﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Account
{
    public class LoginWith2faViewModel
	{
		[Required(ErrorMessage = "Pole wymagane.")]
		[StringLength(7, ErrorMessage = "{0} musi zawierać od {2} do {1} znaków.", MinimumLength = 6)]
        [DataType(DataType.Text)]
        [Display(Name = "Kod Weryfikacyjny")]
        public string TwoFactorCode { get; set; }

        [Display(Name = "Zapamiętaj ten komputer.")]
        public bool RememberMachine { get; set; }

		[Display(Name = "Zapamiętaj mnie.")]
		public bool RememberMe { get; set; }
    }
}
