﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Account
{
    public class LoginWithRecoveryCodeViewModel
	{
		[Required(ErrorMessage = "Pole wymagane.")]
		[DataType(DataType.Text)]
        [Display(Name = "Kod Weryfikacyjny")]
        public string RecoveryCode { get; set; }
    }
}
