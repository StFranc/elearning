﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Account
{
    public class ForgotPasswordViewModel
	{
		[Required(ErrorMessage = "Pole wymagane.")]
		[EmailAddress(ErrorMessage = "Podaj prawidłowy adres email.")]
		public string Email { get; set; }
    }
}
