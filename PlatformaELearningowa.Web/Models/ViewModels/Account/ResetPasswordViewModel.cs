﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Account
{
    public class ResetPasswordViewModel
	{
		[Required(ErrorMessage = "Pole wymagane.")]
		[EmailAddress(ErrorMessage = "Podaj prawidłowy adres email.")]
		public string Email { get; set; }

		[Required(ErrorMessage = "Pole wymagane.")]
		[StringLength(100, ErrorMessage = "{0} musi zawierać od {2} do {1} znaków.", MinimumLength = 6)]
        [DataType(DataType.Password)]
		[Display(Name = "Hasło")]
		public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potwierdzenie Hasła")]
        [Compare("Password", ErrorMessage = "Hasło i hasło potwierdzające nie pasują.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}
