﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Models.ViewModels.Groups
{
    public class GroupWriteVM
	{
		public int GroupId { get; set; }

		[Required(ErrorMessage = "Pole wymagane!")]
		[Display(Name = "Nazwa Grupy")]
		public string Name { get; set; }

		public IEnumerable<CourseSimpleVM> AssociatedCourses { get; set; }
		public IEnumerable<CourseSimpleVM> AvailableCourses { get; set; }
	}
}
