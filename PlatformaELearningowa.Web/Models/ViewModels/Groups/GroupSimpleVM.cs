﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Models.ViewModels.Groups
{
    public class GroupSimpleVM
	{
		public int GroupId { get; set; }

		public string Name { get; set; }

		public virtual ICollection<CourseSimpleVM> AssociatedCourses { get; set; }
	}
}
