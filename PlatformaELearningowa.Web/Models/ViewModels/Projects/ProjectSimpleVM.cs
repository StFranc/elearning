﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Models.ViewModels.Projects
{
    public class ProjectSimpleVM
	{
		public int ProjectId { get; set; }
		
		public virtual CourseSimpleVM Course { get; set; }

		public string Title { get; set; }
	}
}
