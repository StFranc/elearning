﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PlatformaELearningowa.Web.Models.ViewModels.Common;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Models.ViewModels.Projects
{
    public class ProjectStudentVM
	{
		public int ProjectId { get; set; }

		public CourseSimpleVM Course { get; set; }
		
		public string Title { get; set; }

		public string Description { get; set; }
		
		public string PublicTeacherComment { get; set; }

		public FileVM File { get; set; }

		public virtual ProjectFileUploadVM FileUpload { get; set; }
	}
}
