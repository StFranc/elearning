﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PlatformaELearningowa.Web.Models.ViewModels.Projects
{
    public class ProjectFileUploadVM
	{
		public int ProjectId { get; set; }

		[Required(ErrorMessage = "Wybierz Plik")]
		public IFormFile File { get; set; }
	}
}
