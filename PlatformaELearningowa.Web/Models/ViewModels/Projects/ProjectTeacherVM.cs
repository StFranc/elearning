﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Common;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Models.ViewModels.Projects
{
    public class ProjectTeacherVM
	{
		public int? ProjectId { get; set; }
		
		[Required(ErrorMessage = "Pole wymagane!")]
		[Display(Name = "Powiązany Przedmiot")]
		public int CourseId { get; set; }
		public IEnumerable<CourseSimpleVM> AvailableCourses { get; set; }

		public string StudentId { get; set; }

		[Required(ErrorMessage = "Pole wymagane!")]
		[Display(Name = "Nazwa Projektu")]
		public string Title { get; set; }

		[Required(ErrorMessage = "Pole wymagane!")]
		[Display(Name = "Opis Projektu")]
		public string Description { get; set; }

		public FileVM File { get; set; }

		[Display(Name = "Komentarz (Prywatny)")]
		public string PrivateTeacherComment { get; set; }

		[Display(Name = "Komentarz (Dla studenta)")]
		public string PublicTeacherComment { get; set; }

	}
}
