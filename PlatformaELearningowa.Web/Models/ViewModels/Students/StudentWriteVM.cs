﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Groups;
using PlatformaELearningowa.Web.Models.ViewModels.Projects;

namespace PlatformaELearningowa.Web.Models.ViewModels.Students
{
    public class StudentWriteVM
    {
		public string Id { get; set; }

		[Required(ErrorMessage = "Pole wymagane!")]
		[EmailAddress]
		public string Email { get; set; }

		[Display(Name = "Nazwa Studenta")]
		public string UserName { get; set; }

		public IEnumerable<GroupSimpleVM> AssociatedGroups { get; set; }
		public IEnumerable<GroupSimpleVM> AvailableGroups { get; set; }

		public IEnumerable<ProjectSimpleVM> AssignedProjects { get; set; }
	}
}
