﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Students
{
    public class StudentSimpleVM
    {
		public string Email { get; set; }
		public string Id { get; set; }

		[Display(Name = "Nazwa Użytkownika")]
		public string UserName { get; set; }
	}
}
