﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.ViewModels.Announcements;
using PlatformaELearningowa.Web.Models.ViewModels.Common;
using PlatformaELearningowa.Web.Models.ViewModels.Topics;

namespace PlatformaELearningowa.Web.Models.ViewModels.Courses
{
    public class CourseDetailedVM
    {
		public int CourseId { get; set; }

		public String Name { get; set; }

		public String Description { get; set; }

		public IEnumerable<TopicSimpleVM> Topics { get; set; }

		public CourseFileUploadVM FileUploadModel { get; set; }

		public IEnumerable<FileVM> Files { get; set; }

		public IEnumerable<AnnouncementReadVM> Announcements { get; set; }
	}
}
