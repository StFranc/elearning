﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PlatformaELearningowa.Web.Models.ViewModels.Courses
{
    public class CourseWriteVM
    {
		public int? CourseId { get; set; }

		[Required(ErrorMessage = "Pole wymagane.")]
		[DataType(DataType.Text)]
		[Display(Name = "Nazwa")]
		[StringLength(100, ErrorMessage = "{0} musi zawierać od {2} do {1} znaków.", MinimumLength = 6)]
		public string Name { get; set; }

		[Required(ErrorMessage = "Pole wymagane.")]
		[DataType(DataType.MultilineText)]
		[Display(Name = "Opis")]
		[StringLength(3000, ErrorMessage = "{0} musi zawierać od {2} do {1} znaków.", MinimumLength = 6)]
		public string Description { get; set; }

		/*
		[Display(Name = "Pliki")]
		public IFormFileCollection Files { get; set; }
		*/

		[StringLength(100, ErrorMessage = "{0} musi zawierać od {2} do {1} znaków.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Klucz")]
		public string EnrollmentKey { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Potwierdzenie Klucza")]
		[Compare("EnrollmentKey", ErrorMessage = "Klucz i klucz potwierdzający nie pasują.")]
		public string EnrollmentKeyConfirmation { get; set; }

		public string CreatorId { get; set; }
	}
}
