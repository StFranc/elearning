﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PlatformaELearningowa.Web.Models.ViewModels.Courses
{
    public class CourseFileUploadVM
    {
		[Required]
		public int CourseId { get; set; }

		public int? TopicId { get; set; }

		[Required(ErrorMessage = "Wybierz Plik")]
		public IFormFile File { get; set; }
	}
}
