﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Courses
{
    public class CourseSimpleVM
	{
		public int CourseId { get; set; }

		public String Name { get; set; }

		public String Description { get; set; }
	}
}
