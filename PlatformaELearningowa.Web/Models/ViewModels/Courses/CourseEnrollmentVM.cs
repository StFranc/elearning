﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Models.ViewModels.Courses
{
    public class CourseEnrollmentVM
	{
		public int CourseId { get; set; }

		[StringLength(100, ErrorMessage = "{0} musi zawierać od {2} do {1} znaków.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Klucz")]
		public string EnrollmentKey { get; set; }
	}
}
