﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.Entities.EntityMaps;

namespace PlatformaELearningowa.Web.Models
{
	public class ELearningContext : IdentityDbContext<User, Role, String>
	{
		public virtual DbSet<Announcement> Announcements { get; set; }
		public virtual DbSet<Course> Courses { get; set; }
		public virtual DbSet<File> Files { get; set; }
		public virtual DbSet<Group> Groups { get; set; }
		public virtual DbSet<GroupToCourse> GroupsToCourses { get; set; }
		public virtual DbSet<Project> Projects { get; set; }
		public virtual DbSet<Topic> Topics { get; set; }
		public virtual DbSet<UserToCourse> UsersToCourses { get; set; }
		public virtual DbSet<UserToGroup> UsersToGroups { get; set; }
		public virtual DbSet<ProjectFile> ProjectFiles { get; set; }



		public ELearningContext(DbContextOptions<ELearningContext> options) : base(options) { }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			builder.ApplyConfiguration(new CourseMap());
			builder.ApplyConfiguration(new FileMap());
			builder.ApplyConfiguration(new GroupMap());
			builder.ApplyConfiguration(new TopicMap());
			builder.ApplyConfiguration(new UserToCourseMap());
			builder.ApplyConfiguration(new UserToGroupMap());
			builder.ApplyConfiguration(new GroupToCourseMap());
			builder.ApplyConfiguration(new UserMap());
			builder.ApplyConfiguration(new AnnouncementMap());
			builder.ApplyConfiguration(new ProjectMap());
			builder.ApplyConfiguration(new ProjectFileMap());
		}
	}
}
