﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Students;

namespace PlatformaELearningowa.Web.Factories
{
    public interface IStudentsFactory
    {
		StudentSimpleVM CreateSimpleViewModel(User student);
		StudentWriteVM CreateWriteViewModel(User student);
    }
}
