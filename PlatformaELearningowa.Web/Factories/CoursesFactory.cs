﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Factories
{
	public class CoursesFactory : ICoursesFactory
	{
		private readonly IMapper mapper;

		public CoursesFactory(IMapper mapper)
		{
			this.mapper = mapper;
		}

		public Course CreateCourse(CourseWriteVM courseVm)
		{
			var result = mapper.Map<Course>(courseVm);
			result.AssociatedUsers = new List<UserToCourse>();
			return result;
		}

		public CourseEnrollmentVM CreateCourseEnrollmentVM(int courseId, string enrollmentKey)
		{
			var result = new CourseEnrollmentVM
			{ CourseId = courseId, EnrollmentKey = enrollmentKey };
			return result;
		}

		public CourseDetailedVM CreateDetailedViewModel(Course course)
		{
			var result = mapper.Map<CourseDetailedVM>(course);
			return result;
		}

		public CourseSimpleVM CreateSimpleViewModel(Course course)
		{
			var result = mapper.Map<CourseSimpleVM>(course);
			return result;
		}

		public UserToCourse CreateUserToCourseLink(string userId, int courseId)
		{
			var result = new UserToCourse { UserId = userId, CourseId = courseId };
			return result;
		}

		public CourseWriteVM CreateWriteViewModel(Course course)
		{
			var result = mapper.Map<CourseWriteVM>(course);
			return result;
		}
	}
}
