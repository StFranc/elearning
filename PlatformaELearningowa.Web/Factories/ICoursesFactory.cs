﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;

namespace PlatformaELearningowa.Web.Factories
{
    public interface ICoursesFactory
    {
		CourseSimpleVM CreateSimpleViewModel(Course course);

		CourseDetailedVM CreateDetailedViewModel(Course course);

		CourseWriteVM CreateWriteViewModel(Course course);

		Course CreateCourse(CourseWriteVM courseVm);

		UserToCourse CreateUserToCourseLink(string userId, int courseId);

		CourseEnrollmentVM CreateCourseEnrollmentVM(int courseId, string enrollmentKey);
    }
}
