﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Students;

namespace PlatformaELearningowa.Web.Factories
{
	public class StudentsFactory : IStudentsFactory
	{
		private readonly IMapper mapper;

		public StudentsFactory(IMapper mapper)
		{
			this.mapper = mapper;
		}

		public StudentSimpleVM CreateSimpleViewModel(User student)
		{
			var result = mapper.Map<StudentSimpleVM>(student);
			return result;
		}

		public StudentWriteVM CreateWriteViewModel(User student)
		{
			var result = mapper.Map<StudentWriteVM>(student);
			return result;
		}
	}
}
