﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Common;

namespace PlatformaELearningowa.Web.Factories
{
	public class FilesFactory : IFilesFactory
	{
		private readonly IMapper mapper;

		public FilesFactory(IMapper mapper)
		{
			this.mapper = mapper;
		}

		public File CreateFile(IFormFile formFile)
		{
			var result = mapper.Map<File>(formFile);
			return result;
		}

		public IEnumerable<File> CreateFileCollection(IFormFileCollection formFileCollection)
		{
			var result = mapper.Map<IEnumerable<File>>(formFileCollection);
			return result;
		}

		public FileVM CreateFileVM(File file)
		{
			var result = mapper.Map<FileVM>(file);
			return result;
		}
	}
}
