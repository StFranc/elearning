﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;
using PlatformaELearningowa.Web.Models.ViewModels.Topics;

namespace PlatformaELearningowa.Web.Factories
{
    public interface ITopicsFactory
    {
		TopicSimpleVM CreateSimpleViewModel(Topic topic);

		TopicDetailedVM CreateDetailedViewModel(Topic topic);

		TopicWriteVM CreateWriteViewModel(Topic topic);

		TopicWriteVM CreateWriteViewModel(int courseId);

		Topic CreateTopic(TopicWriteVM topic);
	}
}
