﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Common;

namespace PlatformaELearningowa.Web.Factories
{
    public interface IFilesFactory
    {
		File CreateFile(IFormFile formFile);

		IEnumerable<File> CreateFileCollection(IFormFileCollection formFileCollection);

		FileVM CreateFileVM(File file);
    }
}
