﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PlatformaELearningowa.Web.Models.Entities;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;
using PlatformaELearningowa.Web.Models.ViewModels.Topics;

namespace PlatformaELearningowa.Web.Factories
{
	public class TopicsFactory : ITopicsFactory
	{
		private readonly IMapper mapper;

		public TopicsFactory(IMapper mapper)
		{
			this.mapper = mapper;
		}

		public TopicDetailedVM CreateDetailedViewModel(Topic topic)
		{
			var topicVM = mapper.Map<TopicDetailedVM>(topic);
			return topicVM;
		}

		public TopicSimpleVM CreateSimpleViewModel(Topic topic)
		{
			var topicVM = mapper.Map<TopicSimpleVM>(topic);
			return topicVM;
		}

		public Topic CreateTopic(TopicWriteVM topic)
		{
			var topicDomain = mapper.Map<Topic>(topic);
			return topicDomain;
		}

		public TopicWriteVM CreateWriteViewModel(Topic topic)
		{
			var topicVM = mapper.Map<TopicWriteVM>(topic);
			return topicVM;
		}

		public TopicWriteVM CreateWriteViewModel(int courseId)
		{
			return new TopicWriteVM { CourseId = courseId };
		}
	}
}
