﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PlatformaELearningowa.Web.Models.ViewModels.Courses;
using PlatformaELearningowa.Web.Models.ViewModels.Topics;
using PlatformaELearningowa.Web.Models.Entities;
using Microsoft.AspNetCore.Http;
using System.IO;
using ELearningFile = PlatformaELearningowa.Web.Models.Entities.File;
using PlatformaELearningowa.Web.Models.ViewModels.Common;
using MimeTypes.Core;
using PlatformaELearningowa.Web.Models.ViewModels.Students;
using PlatformaELearningowa.Web.Models.ViewModels.Groups;
using PlatformaELearningowa.Web.Models.ViewModels.Projects;
using PlatformaELearningowa.Web.Models.ViewModels.Announcements;

namespace PlatformaELearningowa.Web.Factories
{
    public static class AutomapperConfiguration
	{
		private static byte[] GetByteArrayFromStream(IFormFile formFile)
		{
			byte[] result = null;
			using(MemoryStream ms = new MemoryStream())
			{
				formFile.CopyTo(ms);
				result = ms.ToArray();
			}
			return result;
			
		}

		public static IMapperConfigurationExpression Configure(IMapperConfigurationExpression cfg)
		{
			cfg.CreateMap<IFormFile, ELearningFile>()
				.ForMember(dst => dst.Data, options => options.ResolveUsing(src =>
				GetByteArrayFromStream(src)))
				.ForMember(dst => dst.Name, options => options.ResolveUsing(src =>
				src.FileName));
			cfg.CreateMap<ELearningFile, FileVM>()
				.ForMember(dst => dst.FileExtension, options => options.ResolveUsing(src =>
				MimeTypeMap.GetExtension(src.ContentType)));
			cfg.CreateMap<IFormFile, ProjectFile>()
				.ForMember(dst => dst.Data, options => options.ResolveUsing(src =>
				GetByteArrayFromStream(src)))
				.ForMember(dst => dst.Name, options => options.ResolveUsing(src =>
				src.FileName));
			cfg.CreateMap<ProjectFile, FileVM>()
				.ForMember(dst => dst.FileExtension, options => options.ResolveUsing(src =>
				MimeTypeMap.GetExtension(src.ContentType)))
				.ForMember(dst => dst.FileId, options => options.ResolveUsing(
					src => src.ProjectId));

			cfg.CreateMap<Course, CourseSimpleVM>();
			cfg.CreateMap<Course, CourseDetailedVM>();
			cfg.CreateMap<Course, CourseWriteVM>();
			cfg.CreateMap<CourseWriteVM, Course>();
			
			cfg.CreateMap<Topic, TopicSimpleVM>();
			cfg.CreateMap<Topic, TopicDetailedVM>();
			cfg.CreateMap<Topic, TopicWriteVM>();
			cfg.CreateMap<TopicWriteVM, Topic>();

			cfg.CreateMap<User, StudentSimpleVM>();
			cfg.CreateMap<User, StudentWriteVM>();
			cfg.CreateMap<StudentWriteVM, User>();

			cfg.CreateMap<Group, GroupSimpleVM>();
			cfg.CreateMap<Group, GroupWriteVM>();
			cfg.CreateMap<GroupWriteVM, Group>();

			cfg.CreateMap<Project, ProjectSimpleVM>();
			cfg.CreateMap<Project, ProjectTeacherVM>();
			cfg.CreateMap<Project, ProjectStudentVM>();
			cfg.CreateMap<ProjectTeacherVM, Project>();

			cfg.CreateMap<Announcement, AnnouncementWriteVM>();
			cfg.CreateMap<AnnouncementWriteVM, Announcement>();
			cfg.CreateMap<Announcement, AnnouncementReadVM>();

			return cfg;
		}
    }
}
