﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformaELearningowa.Web.Services
{
	public enum EventType
	{
		DbWriteError,
		DbReadError
	}
}
