﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using PlatformaELearningowa.Web.Models;
using PlatformaELearningowa.Web.Services;
using PlatformaELearningowa.Web.Models.Entities;
using Microsoft.AspNetCore.Identity;
using PlatformaELearningowa.Web.Factories;
using AutoMapper;

namespace PlatformaELearningowa.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.AddDbContext<ELearningContext>(options =>	
				options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
			
			services.AddIdentity<User, Role>()
				.AddEntityFrameworkStores<ELearningContext>()
				.AddDefaultTokenProviders();

			services.AddMvc();
        }

		public void ConfigureContainer(ContainerBuilder builder)
		{
			builder.RegisterType<CoursesService>().As<ICoursesService>();
			builder.RegisterType<CoursesFactory>().As<ICoursesFactory>();

			builder.RegisterType<EmailSender>().As<IEmailSender>();

			builder.RegisterType<NotificationManager>().As<INotificationManager>()
				.InstancePerLifetimeScope();
			builder.RegisterType<NotificationProvider>().As<INotificationProvider>();

			builder.RegisterInstance<IMapper>(new MapperConfiguration(cfg => 
				AutomapperConfiguration.Configure(cfg)).CreateMapper())
				.SingleInstance();

			builder.RegisterType<FilesService>().As<IFilesService>();
			builder.RegisterType<FilesFactory>().As<IFilesFactory>();

			builder.RegisterType<TopicsService>().As<ITopicsService>();
			builder.RegisterType<TopicsFactory>().As<ITopicsFactory>();

			builder.RegisterType<StudentsService>().As<IStudentsService>();
			builder.RegisterType<StudentsFactory>().As<IStudentsFactory>();

			builder.RegisterType<GroupsService>().As<IGroupsService>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

			app.UseAuthentication();

			app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

			RolesData.SeedRoles(app.ApplicationServices).Wait();
		}
    }
}
